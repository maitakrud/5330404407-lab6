<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<body>
			<?php
			$ch = curl_init("www.gotoknow.org/blogs/posts?format=rss");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$content = curl_exec($ch);
			curl_close($ch);
			$doc = new DOMDocument();
			$doc->loadXML($content);
			$channel = $doc->getElementsByTagName('channel')->item(0);
			$nodeItem = $channel->getElementsByTagName('item');

			foreach ($nodeItem as $item) {

				$nodeTitle = $item->getElementsByTagName('title');
				$nodeLink = $item->getElementsByTagName('link');
				$nodeAuthor = $item->getElementsByTagName('author');
				$title = $nodeTitle->item(0)->firstChild->nodeValue;
				$link = $nodeLink->item(0)->firstChild->nodeValue;
				$author = $nodeAuthor->item(0)->firstChild->nodeValue;

				echo "==>item<br/>";
				echo "title =".$title."<br/>";
				echo "link =".$link."<br/>";
				echo "author =".$author."<br/>";
				echo "==>/item<br/>";
			}
		?>
		</body>
	</head>
</html>
