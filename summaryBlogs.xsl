<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <h1>
                    Recent Blog Entries at http://<xsl:value-of select="substring-before(substring-after(items/item/link,'://'), '/')"/>
                </h1>
                <body>
                    <table border="1">
                        <tr bgcolor="#9acd32">
                            <th>Title</th>
                            <th>Author</th>
                            <th>Link</th>
                        </tr>
                        <xsl:for-each select="items/item">
                            <tr>
                                <td>
                                    <xsl:value-of select="title"/>
                                </td>
                                <td>
                                    <xsl:value-of select="author"/>
                                </td>
                                <td>
                                    <xsl:element name= "a">
                                        <xsl:attribute name = "href">
                                            <xsl:value-of select="link"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="link"/>
                                    </xsl:element>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </table>
                </body>
            </head>
        </html>
    </xsl:template>
</xsl:stylesheet>