<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<body>
			<?php
			$ch = curl_init("www.gotoknow.org/blogs/posts?format=rss");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$content = curl_exec($ch);
			curl_close($ch);
			$doc = new DOMDocument();
			$doc->loadXML($content);
			$channel = $doc->getElementsByTagName('channel')->item(0);
			$nodeItem = $channel->getElementsByTagName('item');

			$xmlDoc = new DomDocument('1.0','utf-8');
			$elementItems = $xmlDoc->appendChild($xmlDoc->createElement('items'));

			echo "Reading form summaryBlogs.xml...<br/>";

			foreach ($nodeItem as $item) {

				$getTitle = $item->getElementsByTagName('title');
				$getLink = $item->getElementsByTagName('link');
				$getAuthor = $item->getElementsByTagName('author');
				$textTitle = $getTitle->item(0)->firstChild->nodeValue;
				$textLink = $getLink->item(0)->firstChild->nodeValue;
				$textAuthor = $getAuthor->item(0)->firstChild->nodeValue;

				echo $textTitle.$textLink.$textAuthor." ";

				$elementItem = $elementItems->appendChild($xmlDoc->createElement('item'));
				$elementTitle = $elementItem->appendChild($xmlDoc->createElement('title'));
				$elementTitle->appendChild($xmlDoc->createTextNode($textTitle));
				$elementLink = $elementItem->appendChild($xmlDoc->createElement('link'));
				$elementLink->appendChild($xmlDoc->createTextNode($textLink));
				$elementAuthor = $elementItem->appendChild($xmlDoc->createElement('author'));
				$elementAuthor->appendChild($xmlDoc->createTextNode($textAuthor));

			}

			$xmlDoc->formatOutput = true;
			$xmlDoc->save('summaryBlogs.xml'); 

		?>
		</body>
	</head>
</html>
